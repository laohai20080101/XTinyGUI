/**
 * @file config.h
 * @author ATShining (1358745329@qq.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-10
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#pragma once

#define GUI_COLOR_DEEP (16) /*color deep*/
