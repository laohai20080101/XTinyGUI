/**
 * @file x_malloc.c
 * @author ATShining (1358745329@qq.com)
 * @brief 
 * @version 0.1
 * @date 2024-02-10
 * 
 * @copyright Copyright (c) 2024
 * 
 */
#include "x_malloc.h"
#include <stdlib.h>

void *XMalloc(uint32_t size)
{
	return malloc(size);
}

void XFree(void *mem)
{
	free(mem);
}